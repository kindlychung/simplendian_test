#include <iostream>
#include <simplendian.hpp>

int main() {
    bool is_big_endian = sipmlendian::is_big();
    std::cout << (is_big_endian ? "Big" : "Little") << " endian." << std::endl;
    return 0;
}
